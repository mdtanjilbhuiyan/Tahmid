// Fetch data from the first API to get the verse_key
async function fetchRandomVerse() {
  try {
    const response = await fetch('https://api.quran.com/api/v4/verses/random');
    const data = await response.json();
    return data.verse.verse_key;
  } catch (error) {
    console.error('Error fetching random verse:', error);
    return null;
  }
}

// Fetch verse details using the verse_key from the second API
async function fetchVerseDetails(verseKey) {
  try {
    const response = await fetch(`https://api.quran.com/api/v4/verses/by_key/${verseKey}?language=bn&words=true`);
    const data = await response.json();
    return data.verse.words; // Return the "words" array from the response
  } catch (error) {
    console.error('Error fetching verse details:', error);
    return null;
  }
}

// // Update the content on the new tab with the fetched verse details
// async function updateNewTabContent() {
//   const verseKey = await fetchRandomVerse();
//   if (verseKey) {
//     const verseDetails = await fetchVerseDetails(verseKey);
//     if (verseDetails) {
//       const loadingElement = document.getElementById('loading');
//       loadingElement.style.display = 'none';

//       const verseElement = document.getElementById('verse');
//       const verseText = verseDetails
//         .map(word => word.translation.text)
//         .slice(0, -1) // Exclude the last element
//         .join(' ');

//       verseElement.textContent = verseText;
//       verseElement.classList.remove('hidden');
//     }
//   }
// }

// Fetch Indo-Pak translation using the verse_key
async function fetchIndoPakTranslation(verseKey) {
  try {
    const response = await fetch(`https://api.quran.com/api/v4/quran/verses/indopak?verse_key=${verseKey}`);
    const data = await response.json();
    return data;
  } catch (error) {
    console.error('Error fetching Indo-Pak translation:', error);
    return null;
  }
}

// Update the content on the new tab with the fetched verse details
async function updateNewTabContent() {
  const verseKey = await fetchRandomVerse();
  if (verseKey) {
    try {
      const [verseDetails, indoPakTranslation] = await Promise.all([
        fetchVerseDetails(verseKey),
        fetchIndoPakTranslation(verseKey),
      ]);

      if (verseDetails && indoPakTranslation && indoPakTranslation.verses && indoPakTranslation.verses.length > 0) {
        const loadingElement = document.getElementById('loading');
        loadingElement.style.display = 'none';

        const verseElement = document.getElementById('verse');
        const indoPakElement = document.getElementById('indoPakTranslation');

        // Extract and join the translation text for each word (Indo-Pak)
        const indoPakText = indoPakTranslation.verses.map(verse => verse.text_indopak).join(' ');

        // Set the content for the Indo-Pak translation
        indoPakElement.textContent = `"${indoPakText}"`;
        indoPakElement.classList.remove('hidden');

        // Extract and join the translation text for each word (default translation)
        const verseText = verseDetails
          .map(word => word.translation.text)
          .slice(0, -1) // Exclude the last element
          .join(' ');

        // Add the verse key in parentheses at the end of the verse text
        const fullVerseText = `${verseText} (${verseKey})`;
        verseElement.textContent = fullVerseText;
        verseElement.classList.remove('hidden');
      } else {
        console.error('Error: Missing data from API responses');
      }
    } catch (error) {
      console.error('Error updating new tab content:', error);
    }
  } else {
    console.error('Error: Missing verse key');
  }
}

// Call the function when the new tab is loaded
updateNewTabContent();